package com.example.roomcoba

import android.util.Patterns
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.roomcoba.db.Subscriber
import com.example.roomcoba.db.SubscriberRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.regex.Pattern

class SubscriberViewModel(private val repository: SubscriberRepository) : ViewModel(),Observable {

    val subscriber = repository.subscriber
    private var isUpdateOrDelete = false
    private lateinit var subscriberToUpdateOrDelete : Subscriber

    @Bindable
    val inputName = MutableLiveData<String>()
    @Bindable
    val inputEmail = MutableLiveData<String>()
    @Bindable
    val saveBtnText = MutableLiveData<String>()
    @Bindable
    val clearBtnText = MutableLiveData<String>()

    private val statusMessage = MutableLiveData<Event<String>>()

    // getter because private status message
    val message : LiveData<Event<String>>
        get() = statusMessage

    //constructor
    init {
        saveBtnText.value = "Save"
        clearBtnText.value = "Clear"
    }

    fun saveOrUpdate () {

        if (inputName.value==null) {
            statusMessage.value = Event("Please enter subscriber name")
        } else if (inputEmail.value==null) {
            statusMessage.value = Event("Please enter subscriber email")
        } else if (Patterns.EMAIL_ADDRESS.matcher(inputEmail.value!!).matches()) {
            statusMessage.value = Event("Please enter a correct email address")
        } else {

            if (isUpdateOrDelete) {
                subscriberToUpdateOrDelete.name = inputName.value!!
                subscriberToUpdateOrDelete.email = inputEmail.value!!
                update(subscriberToUpdateOrDelete)
            } else {
                val name = inputName.value!!
                val email = inputEmail.value!!
                insert(Subscriber(0, name, email))
                inputName.value = null
                inputEmail.value = null
            }
        }


    }

    fun clearOrDelete() {

        if (isUpdateOrDelete) {
            delete(subscriberToUpdateOrDelete)
        } else {
            clearAll()
        }
    }

    fun insert(subscriber: Subscriber) : Job = viewModelScope.launch {
            val newRowId = repository.insert(subscriber)
            if (newRowId>-1) {
                statusMessage.value = Event("Subscriber inserted successfully $newRowId")
            } else {
                statusMessage.value = Event("Error occured")
            }
        }
    fun update(subscriber: Subscriber) : Job = viewModelScope.launch {
        val noOfRow = repository.update(subscriber)
        if (noOfRow>-1) {
            inputName.value = null
            inputEmail.value = null
            isUpdateOrDelete = false
            saveBtnText.value = "Save"
            clearBtnText.value = "Clear all"
            statusMessage.value = Event("$noOfRow updated successfully")
        } else {
            statusMessage.value = Event("Error occurred")
        }
    }

    fun delete(subscriber: Subscriber) : Job = viewModelScope.launch {
        val noOfrowDeleted = repository.delete(subscriber)
        if (noOfrowDeleted>0) {
            inputName.value = null
            inputEmail.value = null
            isUpdateOrDelete = false
            saveBtnText.value = "Save"
            clearBtnText.value = "Clear all"
            statusMessage.value = Event("$noOfrowDeleted Row deleted successfully")
        } else {
            statusMessage.value = Event("Error occured")
        }
    }

    fun clearAll () : Job = viewModelScope.launch {
        val noOfRowDeleted = repository.deleteAll()
        if (noOfRowDeleted>0) {
            statusMessage.value = Event("$noOfRowDeleted subscribers deleted successfully")
        } else {
            statusMessage.value = Event("Error occured")
        }
    }

    fun initUpdateAndDelete(subscriber: Subscriber) {
        inputName.value = subscriber.name
        inputEmail.value = subscriber.email
        isUpdateOrDelete = true
        subscriberToUpdateOrDelete = subscriber
        saveBtnText.value = "Update"
        clearBtnText.value = "Delete"
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {

    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
   
    }
}