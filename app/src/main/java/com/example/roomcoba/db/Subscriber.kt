package com.example.roomcoba.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "subscriber_data_table")
data class Subscriber (

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "subsriber_id")
    var id : Int,

    @ColumnInfo(name = "subsriber_name")
    var name : String,

    @ColumnInfo(name = "subsriber_email")
    var email : String
)